const { override, addWebpackAlias } = require('customize-cra');
const path = require('path');

module.exports = override(
  addWebpackAlias({
    ['tbc']: path.resolve(__dirname, './src/components/tbc'),
    ['actions']: path.resolve(__dirname, './src/redux/actions'),
    ['assets']: path.resolve(__dirname, './src/assets'),
  })
);
