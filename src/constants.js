module.exports = {
  PLACEHOLDER_COLOR: '#cacaca',
  BLOG_CONTENT_TYPE: {
    IMAGE: 'image',
    TEXT: 'text',
  },
  SOCIAL_MEDIA: {
    FB: 'fb',
    IG: 'ig',
  },
  HOME_HEADER_SOURCE: {
    BLOGS: 'blogs',
    PRODUCTS: 'products',
    MERCHANDISES: 'merchandises',
    LOCATIONS: 'locations',
  },
  ROUTES_ROOT_PATH: {
    PRODUCTS: '/products',
    BLOGS: '/blogs',
    LOCATIONS: '/locations',
  },
  SOCIAL_MEDIA_LINK: {
    IG: 'https://www.instagram.com/breakfastclubjkt/',
  },
};
