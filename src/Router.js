import React, { lazy } from 'react';
import { Route, Switch, Router } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { history } from './history';

import { ROUTES_ROOT_PATH } from './constants';

// Route-based code splitting
const error404 = lazy(() => import('./views/misc/error/404'));
// const maintenance = lazy(() => import('./views/misc/Maintenance'));
const about = lazy(() => import('./views/About'));
const productRouter = lazy(() => import('./views/Product'));
const blogRouter = lazy(() => import('./views/Blog'));
const contact = lazy(() => import('./views/Contact'));
const locations = lazy(() => import('./views/Locations'));
const home = lazy(() => import('./views/Home'));

function AppRouter() {
  return (
    // Set the directory path if you are deploying in sub-folder
    <ConnectedRouter history={history}>
      <Router history={history}>
        <Switch>
          <Route exact path="/" component={home} />
          <Route path="/about" component={about} />
          <Route path={ROUTES_ROOT_PATH.PRODUCTS} component={productRouter} />
          <Route path={ROUTES_ROOT_PATH.BLOGS} component={blogRouter} />
          <Route path="/contact" component={contact} />
          <Route path={ROUTES_ROOT_PATH.LOCATIONS} component={locations} />

          <Route component={error404} />
        </Switch>
      </Router>
    </ConnectedRouter>
  );
}

export default AppRouter;
