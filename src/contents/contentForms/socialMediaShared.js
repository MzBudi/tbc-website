const socialMediaShared = [
  {
    source: 'ig',
    image: 'CG9g3-uAbkx.webp',
    url: 'https://www.instagram.com/p/CG9g3-uAbkx/',
  },
  {
    source: 'ig',
    image: 'CG9hKrfg8M1.webp',
    url: 'https://www.instagram.com/p/CG9hKrfg8M1/',
  },
  {
    source: 'ig',
    image: 'CHAFjfEAAqX.webp',
    url: 'https://www.instagram.com/p/CHAFjfEAAqX/',
  },
  {
    source: 'ig',
    image: 'CHy-6kagYKs.webp',
    url: 'https://www.instagram.com/p/CHy-6kagYKs/',
  },
  {
    source: 'ig',
    image: 'CHyyQBTgJzH.webp',
    url: 'https://www.instagram.com/p/CHyyQBTgJzH/',
  },
];

export default socialMediaShared;
