const storeLocationContents = [
  {
    name: 'Discovery Lumina - Bintaro',
    location: {
      lat: '-6.2856827',
      lon: '106.6750598',
    },
    image: 'lumina.webp',
    addressLine: 'Jl. Parigi, Parigi, Kec. Pd. Aren, Kota Tangerang Selatan, Banten 15227',
    openHours: {
      hours: '9am - 10pm',
      frequency: 'Open daily',
    },
    description: 'Branch office The Breakfast Club',
  },
  {
    name: 'Main Office The Breakfast Club',
    location: {
      lat: '-6.2231456',
      lon: '106.7809525',
    },
    image: 'bellezza.webp',
    addressLine:
      'Belleza BSA, 1st Floor Unit 106, Jl. Letjen Soepeno,\n' +
      'RT 004 / RW 002, Kelurahan Grogol Utara,\n' +
      'Kecamatan Kebayoran Lama, Jakarta Selatan 12210',
    openHours: {
      hours: '9am - 10pm',
      frequency: 'Open daily',
    },
    description: 'Main office PT. Kreativitas Karya Indonesia',
  },
];

export default storeLocationContents;
