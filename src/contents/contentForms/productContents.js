const productContents = [
  {
    key: 'twinings',
    name: 'Twinings Earl Grey Milk Tea',
    price: 'IDR 45k / 500ml',
    description:
      'Made with fresh ingredients. Aroma dan rasa yang khas dari Twinings Earl Grey Tea, dipadukan' +
      ' dengan susu segar & disajikan dingin dengan segelas es batu. Cocok untuk menemani harimu disegala situasi.' +
      ' Segera konsumsi setelah segel terbuka. Simpan disuhu dingin, dapat bertahan hingga 7 hari',
    specification: {
      'Volume / Price': ['250 ml / 27k', '500 ml / 45k', '1000 ml / 85k'],
    },
    images: ['twinnings_1.webp', 'twinnings_2.webp', 'twinnings_3.webp'],
  },
  {
    key: 'iced-biscoff-caramel-cookie',
    name: 'Iced Biscoff Caramel Cookie',
    price: 'IDR 55k / 500ml',
    description: 'Belum ada deskripsi produk',
    specification: {
      'Volume / Price': ['250 ml / 32k', '500 ml / 55k', '1000 ml / 95k'],
    },
    images: ['biscoff_3.webp', 'biscoff_2.webp', 'biscoff_1.webp'],
  },
  {
    key: 'iced-baileys-cream',
    name: 'Iced Baileys Cream',
    price: 'IDR 55k / 500ml',
    description: 'Belum ada deskripsi produk',
    specification: {
      'Volume / Price': ['250 ml / 32k', '500 ml / 55k', '1000 ml / 95k'],
    },
    images: ['baileys_1.webp', 'baileys_2.webp', 'baileys_3.webp'],
  },
  {
    key: 'froot-loops',
    name: 'Froot Loops',
    price: 'IDR 55k / 500ml',
    description: 'Belum ada deskripsi produk',
    specification: {
      'Volume / Price': ['250 ml / 32k', '500 ml / 55k', '1000 ml / 95k'],
    },
    images: ['frootLoops_1.webp', 'frootLoops_2.webp', 'frootLoops_3.webp'],
  },
  {
    key: 'honey-stars',
    name: 'Honey Stars',
    price: 'IDR 55k / 500ml',
    description: 'Belum ada deskripsi produk',
    specification: {
      'Volume / Price': ['250 ml / 32k', '500 ml / 55k', '1000 ml / 95k'],
    },
    images: ['honeyStars_2.webp', 'honeyStars_1.webp'],
  },
];

export default productContents;
