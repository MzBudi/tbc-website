const productCategories = [
  {
    text: 'Tea Variants',
    key: 'milk-tea-variants',
  },
  {
    text: 'Cookies & Coffee Variants',
    key: 'cookies-coffee-variants',
  },
  {
    text: 'Cereal Infused Milk',
    key: 'cereal-infused-milk',
  },
];

export default productCategories;
