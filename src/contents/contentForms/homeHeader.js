const homeHeader = [
  {
    source: 'products',
    key: 'twinings',
    title: 'Twinings Earl Grey Milk Tea',
    subtitle: 'Milk Tea Variants',
  },
  {
    source: 'products',
    key: 'iced-biscoff-caramel-cookie',
    title: 'Iced Biscoff Caramel Cookie',
    subtitle: 'Coffee Variants',
  },
  {
    source: 'products',
    key: 'froot-loops',
    title: 'Froot Loops',
    subtitle: 'Infused Cereal Milk',
  },
  {
    source: 'blogs',
    key: 'about tbc',
    title: 'Who we are...',
    subtitle: 'The Breakfast Club Histories',
  },
  {
    source: 'merchandises',
    key: 'M1',
    title: 'Special Christmas Hampers',
    subtitle: 'Merchandise',
  },
  {
    source: 'locations',
    key: 'Discovery Lumina - Bintaro',
    title: 'Branch office The Breakfast Club',
    subtitle: 'The Breakfast Club',
  },
];

export default homeHeader;
