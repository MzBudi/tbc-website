const blogContentsTiles = [
  {
    blogKey: 'about TBC',
    publishTime: 1614393999, // Time epoch
    title: {
      text: 'Who we are...',
      style: {},
    },
    subtitle: {
      text: 'The Breakfast Club Histories',
      style: {},
    },
    previewImage: 'about_1.webp',
  },
];

export default blogContentsTiles;
