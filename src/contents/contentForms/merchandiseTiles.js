const merchandiseTiles = [
  {
    merchandiseKey: 'M1',
    name: 'Special Christmas Hampers',
    description:
      'Special Christmas Hampers! 🎁🎅🏻🎄 Limited untuk 50 pembeli pertama hanya 69k! Woohooo 🥳\n' +
      'What you get on Christmas morning:\n' +
      '- Designed Canvas Bag\n' +
      '- The Breakfast Club Signature Flavor 500ml\n' +
      '- Kraft Cup of International Cereal\n' +
      '- Your choice of Soy/Fresh milk 250ml\n' +
      '- Wooden Spoon\n' +
      '- Customize greeting card\n' +
      '\n' +
      'Yuk tunggu apa lagi? Order sebelum kehabisan! ♥️',
    previewImage: '132145684_728966091070040_5499050405775971851_n.webp',
  },
  {
    merchandiseKey: 'M2',
    name: 'Merchandise 2',
    description: 'Made in USA',
    previewImage: 'merchandise_2.webp',
  },
  {
    merchandiseKey: 'M3',
    name: 'Merchandise 3',
    description: 'Made in USA',
    previewImage: 'merchandise_3.webp',
  },
  {
    merchandiseKey: 'M4',
    name: 'Merchandise 4',
    description: 'Made in USA',
    previewImage: 'merchandise_4.webp',
  },
  {
    merchandiseKey: 'M5',
    name: 'Merchandise 5',
    description: 'Made in USA',
    previewImage: 'merchandise_1.webp',
  },
];

export default merchandiseTiles;
