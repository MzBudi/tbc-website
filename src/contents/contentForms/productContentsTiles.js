const productContentsTiles = [
  {
    productKey: 'twinings',
    previewImage: 'twinnings_1.webp',
    categoryKey: 'milk-tea-variants',
  },
  {
    productKey: 'iced-biscoff-caramel-cookie',
    previewImage: 'biscoff_1.webp',
    categoryKey: 'cookies-coffee-variants',
  },
  {
    productKey: 'iced-baileys-cream',
    previewImage: 'baileys_1.webp',
    categoryKey: 'cookies-coffee-variants',
  },
  {
    productKey: 'froot-loops',
    previewImage: 'frootLoops_1.webp',
    categoryKey: 'cereal-infused-milk',
  },
  {
    productKey: 'honey-stars',
    previewImage: 'honeyStars_1.webp',
    categoryKey: 'cereal-infused-milk',
  },
];

export default productContentsTiles;
