const aboutTBC = {
  key: 'about TBC',
  header: {
    title: {
      text: 'Who we are...',
      style: {
        fontFamily: 'sf ui display',
        textAlign: 'right',
      },
    },
    previewImage: 'about_1.webp',
  },
  contents: [
    {
      type: 'text',
      content: 'Who we are...',
      style: {
        fontSize: '2rem',
        lineHeight: '2rem',
        textAlign: 'center',
        fontWeight: 'bold',
      },
    },
    {
      type: 'text',
      content:
        'The Breakfast Club mengadopsi konsep fresh to cup yang menyajikan produk berbahan dasar susu dengan berbagai\n' +
        'pilihan rasa yang unik dan menarik serta harga yang terjangkau. Pemilihan bahan baku terbaik di setiap\n' +
        'menunya dan proses produksi yang efektif adalah prinsip kami. Penyajian yang modern dan higenis, serta\n' +
        'selalu mengedepankan cita rasa dan menjaga kualitas mutu adalah tujuan kami. Hal tersebut membuat produk\n' +
        'kami begitu di cintai masyarakat dan terus berkembang seiring waktu.',
      style: {
        textAlign: 'center',
      },
    },
    {
      type: 'image',
      content: 'logo.png',
      style: {
        textAlign: 'center',
      },
    },
    {
      type: 'text',
      content:
        'Visi The Breakfast Club adalah meningkatkan brand awareness, menguatkan pondasi brand position sebagai\n' +
        '            pioneer produk berbahan dasar susu dengan menu yang belum pernah ada sebelumnya, serta menjadi pelopor\n' +
        '            gerakan minum susu untuk meningkatkan kualitas kesehatan anak bangsa. Kedepannya kami akan mengembangkan\n' +
        '            visi dan misi tersebut melalui konsep waralaba. Kami berharap The Breakfast Club dapat ikut andil\n' +
        '            menciptakan lapangan kerja dan menjadi platform untuk anak bangsa berkarya.',
      style: {
        textAlign: 'center',
      },
    },
    {
      type: 'image',
      content: 'about_2.webp',
      style: {
        textAlign: 'center',
      },
    },
  ],
};

export default aboutTBC;
