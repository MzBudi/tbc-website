import Validator from 'fastest-validator';
import kebabCase from 'lodash.kebabcase';
import storeLocationContents from '../contentForms/storeLocationContents';

const validator = new Validator();

const storeLocationContentsValidator = validator.compile({
  name: { type: 'string', min: 1, max: 64 },
  location: {
    $$type: 'object',
    lat: { type: 'string', numeric: true, max: 16 },
    lon: { type: 'string', numeric: true, max: 16 },
  },
  image: { type: 'string', min: 8, max: 255 },
  addressLine: { type: 'string', min: 8, max: 255 },
  openHours: {
    $$type: 'object',
    hours: { type: 'string', max: 32 },
    frequency: { type: 'string', max: 32 },
  },
  description: { type: 'string', min: 1, max: 1024, optional: true },
  $$strict: true,
});

const __storeLocationContents = {};
const _storeLocationContents = storeLocationContents.reduce((accum, content) => {
  const isValid = storeLocationContentsValidator(content);
  if (isValid !== true)
    throw new Error(
      JSON.stringify({
        source: 'storeLocationContents',
        isValid,
      })
    );
  content.key = kebabCase(content.name);
  accum[content.key] = content;

  __storeLocationContents[content.key] = {
    previewImage: content.image,
  };
  return accum;
}, {});

export const __storeLocationTiles = __storeLocationContents;

export default _storeLocationContents;
