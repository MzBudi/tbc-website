import Validator from 'fastest-validator';
import socialMediaShared from '../contentForms/socialMediaShared';

const validator = new Validator();

const socialMediaSharedValidator = validator.compile({
  source: { type: 'enum', values: ['fb', 'ig'], optional: true },
  image: { type: 'string', min: 8, max: 255, optional: true },
  url: { type: 'url', default: '/' },
  $$strict: true,
});

socialMediaShared.forEach((content) => {
  const isValid = socialMediaSharedValidator(content);
  if (isValid !== true)
    throw new Error(
      JSON.stringify({
        source: 'socialMediaShared',
        isValid,
      })
    );
});

export default socialMediaShared;
