import Validator from 'fastest-validator';
import kebabCase from 'lodash.kebabcase';
import blogTiles from '../contentForms/blogContentsTiles';
import { availableFonts } from '../../configs/styleConfig';

const validator = new Validator();

const blogTilesValidator = validator.compile({
  blogKey: { type: 'string', min: 1, max: 32 },
  publishTime: { type: 'number' },
  title: {
    $$type: 'object',
    text: { type: 'string', min: 4, max: 128 },
    style: {
      $$type: 'object',
      fontFamily: { type: 'enum', lowercase: true, values: Object.keys(availableFonts), optional: true },
      color: { type: 'string', min: 4, max: 8, optional: true },
    },
  },
  subtitle: {
    $$type: 'object',
    text: { type: 'string', min: 4, max: 128 },
    style: {
      $$type: 'object',
      fontFamily: { type: 'enum', lowercase: true, values: Object.keys(availableFonts), optional: true },
      color: { type: 'string', min: 4, max: 8, optional: true },
    },
  },
  previewImage: { type: 'string', min: 8, max: 255, optional: true },
  $$strict: true,
});

const _blogTiles = blogTiles.map((content) => {
  const isValid = blogTilesValidator(content);
  if (isValid !== true)
    throw new Error(
      JSON.stringify({
        source: 'blogContentsTiles',
        isValid,
      })
    );
  const { title, subtitle, blogKey } = content;
  content.blogKey = kebabCase(blogKey);
  content.title.style.fontFamily = availableFonts[title.style.fontFamily];
  content.subtitle.style.fontFamily = availableFonts[subtitle.style.fontFamily];
  return content;
});

export const __blogTiles = blogTiles.reduce((prev, { blogKey, previewImage }) => {
  prev[kebabCase(blogKey)] = {
    previewImage,
  };
  return prev;
}, {});

export default _blogTiles;
