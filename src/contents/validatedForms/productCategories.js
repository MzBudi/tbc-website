import Validator from 'fastest-validator';
import kebabCase from 'lodash.kebabcase';
import productCategories from '../contentForms/productCategories';

const validator = new Validator();

const productCategoriesValidator = validator.compile({
  key: { type: 'string', min: 1, max: 32 },
  text: { type: 'string', min: 1, max: 32 },
  $$strict: true,
});

const _productCategories = productCategories.map((content) => {
  const isValid = productCategoriesValidator(content);
  if (isValid !== true)
    throw new Error(
      JSON.stringify({
        source: 'productCategories',
        isValid,
      })
    );

  content.key = kebabCase(content.key);
  return content;
});

export default _productCategories;
