import Validator from 'fastest-validator';
import kebabCase from 'lodash.kebabcase';
import { availableFonts, allowedFontStyle, allowedFontWeight, allowedTextAlign } from '../../configs/styleConfig';
import blogContents from '../contentForms/blogContents';
import { BLOG_CONTENT_TYPE } from '../../constants';

const validator = new Validator();

const blogContentsValidator = validator.compile({
  key: { type: 'string', min: 1, max: 32 },
  header: {
    $$type: 'object',
    title: {
      $$type: 'object',
      text: { type: 'string', min: 4, max: 128 },
      style: {
        $$type: 'object',
        fontFamily: { type: 'enum', lowercase: true, values: Object.keys(availableFonts), optional: true },
        fontWeight: { type: 'enum', lowercase: true, values: allowedFontWeight, optional: true },
        fontStyle: { type: 'enum', lowercase: true, values: allowedFontStyle, optional: true },
        color: { type: 'string', min: 4, max: 8, optional: true },
      },
    },
    previewImage: { type: 'string', min: 8, max: 255 },
  },
  contents: {
    type: 'array',
    items: {
      type: 'object',
      strict: true,
      props: {
        type: { type: 'enum', values: Object.values(BLOG_CONTENT_TYPE), lowercase: true },
        content: { type: 'string', min: 1, max: '1024' },
        style: {
          $$type: 'object',
          textAlign: { type: 'enum', values: allowedTextAlign, optional: true },
          fontSize: { type: 'string', min: 3, max: 8, optional: true },
          fontFamily: { type: 'enum', lowercase: true, values: Object.keys(availableFonts), optional: true },
          fontWeight: { type: 'enum', lowercase: true, values: allowedFontWeight, optional: true },
          fontStyle: { type: 'enum', lowercase: true, values: allowedFontStyle, optional: true },
          lineHeight: { type: 'string', min: 4, max: 8, optional: true },
          color: { type: 'string', min: 4, max: 8, optional: true },
        },
      },
    },
  },
  $$strict: true,
});

const _blogContents = blogContents.reduce((accum, content) => {
  const isValid = blogContentsValidator(content);
  if (isValid !== true)
    throw new Error(
      JSON.stringify({
        source: 'blogContents',
        isValid,
      })
    );
  const {
    key,
    header: { title },
    contents,
  } = content;

  content.key = kebabCase(key);

  content.header.title.style.fontFamily = availableFonts[title.style.fontFamily];
  content.contents = contents.map((item) => {
    item.style.fontFamily = availableFonts[item.style.fontFamily];
    return item;
  });

  accum[content.key] = content;
  return accum;
}, {});

export default _blogContents;
