import Validator from 'fastest-validator';
import kebabCase from 'lodash.kebabcase';
import homeHeader from '../contentForms/homeHeader';
import { HOME_HEADER_SOURCE } from '../../constants';
import { __blogTiles } from './blogTiles';
import { __productTiles } from './productTiles';
import { __merchandiseTiles } from './merchandiseTiles';
import { __storeLocationTiles } from './storeLocationContents';

const validator = new Validator();

const landingHeaderValidator = validator.compile({
  source: { type: 'enum', values: Object.values(HOME_HEADER_SOURCE) },
  key: { type: 'string', min: 1, max: 32 },
  title: { type: 'string', min: 4, max: 128 },
  subtitle: { type: 'string', min: 4, max: 128 },
  $$strict: true,
});

const _homeHeader = homeHeader.map((content) => {
  const isValid = landingHeaderValidator(content);
  if (isValid !== true)
    throw new Error(
      JSON.stringify({
        source: 'homeHeader',
        isValid,
      })
    );

  content.key = kebabCase(content.key);
  const data = {
    headerKey: content.key,
    ...content,
  };
  switch (content.source) {
    case HOME_HEADER_SOURCE.BLOGS:
      Object.assign(data, __blogTiles[content.key]);
      break;
    case HOME_HEADER_SOURCE.PRODUCTS:
      Object.assign(data, __productTiles[content.key]);
      break;
    case HOME_HEADER_SOURCE.MERCHANDISES:
      Object.assign(data, __merchandiseTiles[content.key]);
      break;
    case HOME_HEADER_SOURCE.LOCATIONS:
      Object.assign(data, __storeLocationTiles[content.key]);
      break;
    default:
      break;
  }

  return data;
});

export default _homeHeader;
