import Validator from 'fastest-validator';
import kebabCase from 'lodash.kebabcase';
import productContents from '../contentForms/productContents';

const validator = new Validator();

const productContentsValidator = validator.compile({
  key: { type: 'string', min: 1, max: 32 },
  name: { type: 'string', min: 1, max: 32 },
  price: { type: 'string', min: 1, max: 32 },
  description: { type: 'string', min: 1, max: 512 },
  specification: { type: 'object' },
  images: { type: 'array' },
  $$strict: true,
});

const _productContents = productContents.reduce((accum, content) => {
  const isValid = productContentsValidator(content);
  if (isValid !== true)
    throw new Error(
      JSON.stringify({
        source: 'productContentsTiles',
        isValid,
      })
    );

  const { key } = content;
  content.key = kebabCase(key);
  accum[key] = content;
  return accum;
}, {});

export default _productContents;
