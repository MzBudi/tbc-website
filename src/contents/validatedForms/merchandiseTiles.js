import Validator from 'fastest-validator';
import kebabCase from 'lodash.kebabcase';
import merchandiseTiles from '../contentForms/merchandiseTiles';

const validator = new Validator();

const merchandiseTilesValidator = validator.compile({
  merchandiseKey: { type: 'string', min: 1, max: 32 },
  name: { type: 'string', min: 1, max: 32 },
  description: { type: 'string', min: 1, max: 512 },
  previewImage: { type: 'string', min: 8, max: 255, optional: true },
  $$strict: true,
});

const _merchandiseTiles = merchandiseTiles.reduce((prev, content) => {
  const isValid = merchandiseTilesValidator(content);
  if (isValid !== true)
    throw new Error(
      JSON.stringify({
        source: 'merchandiseTiles',
        isValid,
      })
    );

  content.merchandiseKey = kebabCase(content.merchandiseKey);
  prev[content.merchandiseKey] = content;

  return prev;
}, {});

export const __merchandiseTiles = merchandiseTiles.reduce((prev, { merchandiseKey, previewImage }) => {
  prev[kebabCase(merchandiseKey)] = {
    previewImage,
  };

  return prev;
}, {});

export default _merchandiseTiles;
