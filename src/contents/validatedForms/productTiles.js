import Validator from 'fastest-validator';
import kebabCase from 'lodash.kebabcase';
import productTiles from '../contentForms/productContentsTiles';

const validator = new Validator();

const productTilesValidator = validator.compile({
  productKey: { type: 'string', min: 1, max: 32 },
  categoryKey: { type: 'string', min: 1, max: 32 },
  previewImage: { type: 'string', min: 8, max: 255, optional: true },
  $$strict: true,
});

const ___productTiles = {};
const _productTiles = productTiles.reduce((accum, content) => {
  const isValid = productTilesValidator(content);
  if (isValid !== true)
    throw new Error(
      JSON.stringify({
        source: 'productContentsTiles',
        isValid,
      })
    );
  const { categoryKey: _categoryKey, productKey } = content;

  content.productKey = kebabCase(productKey);
  const categoryKey = kebabCase(_categoryKey);

  if (!accum[categoryKey]) {
    accum[categoryKey] = [];
  }
  accum[categoryKey].push(content);

  ___productTiles[content.productKey] = {
    previewImage: content.previewImage,
    metadata: {
      categoryKey,
    },
  };
  return accum;
}, {});

export const __productTiles = ___productTiles;

export default _productTiles;
