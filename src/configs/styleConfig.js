export const availableFonts = {
  'sf ui display': 'SF UI Display',
};

export const allowedFontStyle = ['normal', 'italic', 'oblique'];

export const allowedFontWeight = ['bold', 'bolder', 'normal', 'lighter'];
export const allowedTextAlign = ['left', 'right', 'justify', 'center'];

// // Example calculation
// const currentWidth = window.outerWidth;
// const currentBaseFontSize = (currentWidth / baseWidth) * baseFontSize;
