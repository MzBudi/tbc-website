import React, { Suspense, lazy, useEffect, createRef } from 'react';
import { connect } from 'react-redux';
import { Col, Row } from 'react-bootstrap';
import { LoadingSpinner } from './components/tbc';
import Header from './views/Header';
import Footer from './views/Footer';
import SocialMediaSharedContents from './views/SocialMediaSharedContents';
import PromotionBrochure from './views/PromotionBrochures';

const Router = lazy(() => import('./Router'));

const App = ({ router }) => {
  const appRef = createRef();

  const scrollToTop = () => appRef.current.scrollTo(0, 0);
  useEffect(() => {
    let render = true;
    if (render) {
      scrollToTop();
    }

    return () => {
      render = false;
    };
  }, [router.location.pathname]);

  return (
    <main className="main position-relative" ref={appRef}>
      <Header />
      <Row>
        <Col sm={12} md={12} lg={12} className="def-h-content p-0">
          <Suspense fallback={<LoadingSpinner />}>
            <Router />
          </Suspense>
        </Col>
      </Row>

      <Row>
        <PromotionBrochure />
      </Row>

      <Row>
        <Col sm={12} md={12} lg={12} className="px-0 mx-0 position-relative">
          <SocialMediaSharedContents />
        </Col>
      </Row>

      <Footer />
    </main>
  );
};

const mapStateToProps = (state) => ({
  router: state.router,
});
export default connect(mapStateToProps)(App);
