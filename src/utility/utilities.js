import { customAlphabet } from 'nanoid';
import { useEffect, useState } from 'react';
import { PLACEHOLDER_COLOR } from '../constants';

export function setCookie(cname, cvalue, lifetime) {
  const now = new Date().getTime();
  const laterEpoch = Math.round(now / 1000) + lifetime;
  const later = new Date(0);
  later.setUTCSeconds(laterEpoch);
  const expires = `expires=${later.toUTCString()}`;
  document.cookie = `${cname}=${cvalue};${expires};path=/`;
}

export function getCookie(cname) {
  const name = `${cname}=`;
  const ca = document.cookie.split(';');
  for (let i = 0; i < ca.length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
}

export function deleteAllCookie() {
  const cookies = document.cookie.split(';');

  for (let i = 0; i < cookies.length; i += 1) {
    const cookie = cookies[i];
    const eqPos = cookie.indexOf('=');
    const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
    document.cookie = `${name}=;expires=Thu, 01 Jan 1970 00:00:00 GMT`;
  }
}

export function fromEpoch(sec) {
  const d = new Date(0);
  d.setUTCSeconds(sec);
  return d;
}

export function objectValToKey(obj) {
  Object.keys(obj).forEach((key) => {
    obj[obj[key]] = key;
  });
  return obj;
}

export function generateId(n = 6) {
  return customAlphabet('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz', n)();
}

export const generatePlaceholder = (width, height) =>
  `data:image/svg+xml;base64,${window.btoa(
    `<svg height="${height}" width="${width}" xmlns="http://www.w3.org/2000/svg">
        <rect x="0" y="0" width="${width}" height="${height}" fill="${PLACEHOLDER_COLOR}"/>
      </svg>`
  )}`;

export function useWindowDimensions() {
  const getWindowDimensions = () => {
    const { innerWidth: width, innerHeight: height } = window;
    return {
      width,
      height,
    };
  };

  const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return windowDimensions;
}
