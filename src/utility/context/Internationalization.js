import React from 'react';
import { IntlProvider } from 'react-intl';

import messageId from '../../assets/data/locales/id.json';

const menuMessages = {
  id: messageId,
};

const Context = React.createContext(undefined);

class IntlProviderWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: 'id',
      messages: menuMessages.id,
    };
  }

  render() {
    const { children } = this.props;
    const { locale, messages } = this.state;
    return (
      <Context.Provider
        value={{
          state: this.state,
          switchLanguage: (language) => {
            this.setState({
              locale: language,
              messages: menuMessages[language],
            });
          },
        }}
      >
        <IntlProvider key={locale} locale={locale} messages={messages} defaultLocale="en">
          {children}
        </IntlProvider>
      </Context.Provider>
    );
  }
}

export { IntlProviderWrapper, Context as IntlContext };
