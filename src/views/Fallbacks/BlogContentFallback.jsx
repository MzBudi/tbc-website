import React from 'react';

export default function BlogContentFallback() {
  return (
    <div
      className="text-info font-weight-bold m-2"
      style={{ height: 'fit-content', backgroundColor: 'lightgray', opacity: 0.75, fontSize: '1rem' }}
    >
      IF IMAGE EXIST, HERE WILL SHOW AN IMAGE
    </div>
  );
}
