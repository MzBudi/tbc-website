import React from 'react';
import { Image } from 'react-bootstrap';
import { LoadingSpinner } from 'tbc';
import manifest from '../../assets/img/dummy/xl-1200/manifest.json';
import { generatePlaceholder } from '../../utility/utilities';

const { resolutions } = manifest;

function LocationPreviewImg() {
  const defSize = resolutions['social-media-shared'];
  return (
    <>
      <Image src={generatePlaceholder(...defSize)} alt="dummy" className="img-fluid image-fallback" />
      <LoadingSpinner />
    </>
  );
}

export default LocationPreviewImg;
