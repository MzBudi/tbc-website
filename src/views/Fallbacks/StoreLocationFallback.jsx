import React from 'react';
import { Image } from 'react-bootstrap';
import { LoadingSpinner } from 'tbc';
import manifest from '../../assets/img/dummy/xl-1200/manifest.json';
import { generatePlaceholder } from '../../utility/utilities';

const { resolutions } = manifest;

export function StorePreviewFallback({ fallbackClass, ...props }) {
  const defSize = resolutions['store-preview'];
  return (
    <>
      <Image
        src={generatePlaceholder(...defSize)}
        alt="dummy"
        className={`${fallbackClass} image-fallback`}
        {...props}
      />

      <LoadingSpinner />
    </>
  );
}

export function StoreMainFallback() {
  const defSize = resolutions['store-main'];
  return (
    <>
      <Image src={generatePlaceholder(...defSize)} alt="dummy" className="img-fluid image-fallback" />
      <LoadingSpinner />
    </>
  );
}

export function StoreMapFallback({ className }) {
  const defSize = resolutions['store-map'];
  return (
    <div className={className}>
      <Image src={generatePlaceholder(...defSize)} alt="dummy" className="img-fluid image-fallback" />
      <LoadingSpinner />
    </div>
  );
}
