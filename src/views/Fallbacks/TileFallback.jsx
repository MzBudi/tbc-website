import React from 'react';
import { Image } from 'react-bootstrap';
import { LoadingSpinner } from 'tbc';
import manifest from '../../assets/img/dummy/xl-1200/manifest.json';
import { generatePlaceholder } from '../../utility/utilities';

const { resolutions } = manifest;
const defSize = resolutions.tile;

export default function TileFallback() {
  return (
    <>
      <Image src={generatePlaceholder(...defSize)} className="img-fluid image-fallback" />
      <LoadingSpinner />
    </>
  );
}
