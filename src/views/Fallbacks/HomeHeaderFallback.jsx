import React from 'react';
import { Image } from 'react-bootstrap';
import { LoadingSpinner } from 'tbc';
import manifest from '../../assets/img/dummy/xl-1200/manifest.json';
import { generatePlaceholder } from '../../utility/utilities';

const { resolutions } = manifest;
const defSize = resolutions['home-header'];

export default function HomeHeaderFallback() {
  return (
    <>
      <Image src={generatePlaceholder(...defSize)} className="img-fluid image-fallback" />
      <LoadingSpinner />
    </>
  );
}
