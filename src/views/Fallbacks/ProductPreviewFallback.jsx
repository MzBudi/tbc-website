import React from 'react';
import { LoadingSpinner } from 'tbc';
import { Image } from 'react-bootstrap';
import manifest from '../../assets/img/dummy/xl-1200/manifest.json';
import { generatePlaceholder } from '../../utility/utilities';

const { resolutions } = manifest;
const defSize = resolutions['product-preview'];

export default function ProductPreviewFallback({ fallbackClass, ...props }) {
  return (
    <>
      <Image
        src={generatePlaceholder(defSize[0], defSize[1])}
        alt="dummy"
        className={`${fallbackClass} image-fallback`}
        {...props}
      />
      <LoadingSpinner />
    </>
  );
}
