import React from 'react';
import { connect } from 'react-redux';
import { Navbar, Nav, Container, Image } from 'react-bootstrap';
import backgroundTbc from '../assets/img/logo/logo.png';

function Header({ router }) {
  const {
    location: { pathname },
  } = router;

  return (
    <Navbar variant="light" collapseOnSelect expand="lg" className="navbar-navigation">
      <Container>
        <Navbar.Brand href="#/" className="ml-3">
          <Image src={backgroundTbc} width="100" className="d-inline-block align-top" alt="React Bootstrap logo" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="navbar-navigation-link-spacing ml-3 mt-sm-3 mt-lg-0">
            <Nav.Link eventKey={1} href="#/about" active={pathname.includes('/about')}>
              ABOUT US
            </Nav.Link>
            <Nav.Link eventKey={2} href="#/products" active={pathname.includes('/products')}>
              PRODUCTS
            </Nav.Link>
            <Nav.Link eventKey={3} href="#/blogs" active={pathname.includes('/blogs')}>
              BLOGS
            </Nav.Link>
            <Nav.Link eventKey={4} href="#/locations" active={pathname.includes('/locations')}>
              LOCATIONS
            </Nav.Link>
            <Nav.Link eventKey={5} href="#/contact" active={pathname.includes('/contact')}>
              CONTACT
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

const mapStateToProps = (state) => ({
  router: state.router,
});
export default connect(mapStateToProps)(Header);
