import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { SuspenseImage } from 'tbc';
import Carousel from 'react-multi-carousel';
import { Row, Col, Card } from 'react-bootstrap';
import { merchandiseTiles } from '../contents';
import MerchandisePreviewFallback from './Fallbacks/MerchandisePreviewFallback';
import { useWindowDimensions } from '../utility/utilities';
import { merchandisesSectionRef } from '../refs';

import 'react-multi-carousel/lib/styles.css';

const loadProductContentImage = async (previewImage) => {
  try {
    const result = await import(`../assets/img/pages/merchandises/${previewImage}`);
    return result.default;
  } catch (err) {
    console.warn(err);
  }
};

const ImageContent = ({ alt, imageSource, className }) => {
  const [image, setImage] = useState(undefined);

  useEffect(() => {
    let render = true;
    if (render) {
      loadProductContentImage(imageSource).then((loadedImage) => setImage(loadedImage));
    }

    return () => {
      render = true;
    };
  }, [imageSource]);

  return (
    <SuspenseImage
      src={image}
      fallback={MerchandisePreviewFallback}
      alt={alt || 'The Breakfast Club'}
      className={className}
    />
  );
};

function Merchandise({ router }) {
  const {
    location: { query },
  } = router;

  const randomProperty = (obj) => {
    const keys = Object.keys(obj);
    return obj[`${keys[parseInt(keys.length * Math.random(), 10)]}`] || {};
  };

  const randomContent = randomProperty(merchandiseTiles);

  const [content, setContent] = useState(merchandiseTiles[query.merchandise] || randomContent);

  const { width } = useWindowDimensions();

  useEffect(() => {
    let mounted = true;

    if (mounted) {
      setContent(merchandiseTiles[query.merchandise] || randomContent);
    }

    return () => {
      mounted = false;
    };
  }, [query]);

  const responsive = {
    desktopNarrow: {
      breakpoint: { max: 3000, min: 1366 },
      items: 3,
      slidesToSlide: 1,
    },
    desktop: {
      breakpoint: { max: 1366, min: 1024 },
      items: 3,
      slidesToSlide: 1,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 3,
      slidesToSlide: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      slidesToSlide: 1,
    },
  };

  return (
    <div className="merchandise">
      <div ref={merchandisesSectionRef} className="contact-headline">
        MERCHANDISE
      </div>
      <Row className="w-100 pt-4 mx-0">
        <Col sm={12} md={12} lg={3} className="description">
          <p className="description name">{content.name}</p>
          <p className="description text">{content.description}</p>
        </Col>
        <Col sm={12} md={12} lg={9} className="merchandise-carousel p-0">
          <Carousel
            responsive={responsive}
            additionalTransfrom={0}
            arrows={false}
            autoPlay={width >= 1024}
            infinite
            autoPlaySpeed={5000}
            draggable
            focusOnSelect={false}
            keyBoardControl
            minimumTouchDrag={40}
            renderButtonGroupOutside
            renderDotsOutside
            dotListClass="custom-carousel-dot-list"
            showDots
          >
            {Object.values(merchandiseTiles).map((item, idx) => (
              <Card.Body
                id={item.merchandiseKey}
                key={idx}
                className={classNames('tile animating position-relative', {
                  'even fade-in-down': idx % 2 === 0,
                  'odd fade-in-up': idx % 2 !== 0,
                })}
                onClick={() => setContent(item)}
                role="link"
              >
                <div className="merchandise-name">{item.name}</div>
                <ImageContent alt={item.description} imageSource={item.previewImage} className="merchandise-image" />
              </Card.Body>
            ))}
          </Carousel>
        </Col>
      </Row>
    </div>
  );
}

const mapStateToProps = ({ router }) => ({
  router,
});

export default connect(mapStateToProps)(Merchandise);
