import React from 'react';
import { navigateBack } from 'actions';
import { X, Mail, MessageSquare, MapPin, User } from 'react-feather';
import { Container, Button, Row, Col, Form } from 'react-bootstrap';

export default function FranchiseInquiries() {
  return (
    <Container className="animating zoom-in">
      <div className="contact-headline d-flex justify-content-center align-items-center">
        <span>FRANCHISE INQUIRIES</span>
        <Button onClick={navigateBack} variant="link" color="black" className="ml-5">
          <X color="#3D3D3D" width="33" height="33" />
        </Button>
      </div>
      <p className="sub-head-form pb-5 pt-4 col-xl-6 m-auto">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec felis augue, volutpat ut risus in, facilisis
        mollis metus. Pellentesque aliquam commodo justo ut laoreet. Duis consequat ante at risus lobortis, sit amet
        dictum purus mollis. Pellentesque sed ante augue. Donec at vestibulum velit. Aliquam imperdiet turpis in lorem
        pellentesque ornare. Pellentesque pulvinar augue at ipsum ullamcorper ultrices. Mauris eget accumsan orci, vitae
        vulputate turpis.
      </p>
      <div className="d-flex flex-column align-items-center">
        <Form.Group controlId="formPlaintextName">
          <Row className="mb-3">
            <Col sm={12} md={6} lg={5}>
              <Form.Label className="form-label">
                <User color="#3D3D3D" width="33" height="33" className="pr-2" />
                YOUR NAME
              </Form.Label>
            </Col>
            <Col sm={12} md={6} lg={7}>
              <Form.Control type="text" size="lg" />
            </Col>
          </Row>

          <Row className="mb-3">
            <Col sm={12} md={6} lg={5}>
              <Form.Label className="form-label">
                <Mail color="#3D3D3D" width="33" height="33" className="pr-2" />
                EMAIL ADDRESS
              </Form.Label>
            </Col>

            <Col sm={12} md={6} lg={7}>
              <Form.Control type="email" size="lg" />
            </Col>
          </Row>

          <Row className="mb-3">
            <Col sm={12} md={6} lg={5}>
              <Form.Label className="form-label">
                <MapPin color="#3D3D3D" width="33" height="33" className="pr-2" />
                CITY OF INTEREST
              </Form.Label>
            </Col>
            <Col sm={12} md={6} lg={7}>
              <Form.Control as="select" className="my-1 mr-sm-2" id="inlineFormCustomSelectPref" size="lg">
                <option value="0">Choose...</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </Form.Control>
            </Col>
          </Row>

          <Row className="mb-3">
            <Col sm={12} md={6} lg={5}>
              <Form.Label className="form-label">
                <MessageSquare color="#3D3D3D" width="33" height="33" className="pr-2" />
                YOUR MESSAGE
              </Form.Label>
            </Col>
            <Col sm={12} md={6} lg={7}>
              <Form.Control as="textarea" rows={5} size="lg" />
            </Col>
          </Row>

          <Button variant="dark" block className="btn-submit">
            SEND
          </Button>
        </Form.Group>
      </div>
    </Container>
  );
}
