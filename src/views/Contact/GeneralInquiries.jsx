import React from 'react';
import { navigateBack } from 'actions';
import { X, User, MessageSquare, Mail } from 'react-feather';
import { Container, Button, Row, Col, Form } from 'react-bootstrap';

export default function GeneralInquiries() {
  return (
    <Container className="animating zoom-in pt-5 pb-5">
      <div className="contact-headline d-flex justify-content-center align-items-center">
        <span>GENERAL INQUIRIES</span>
        <Button onClick={navigateBack} variant="link" color="black" className="ml-4 pl-4">
          <X color="#3D3D3D" width="33" height="33" />
        </Button>
      </div>
      <div className="d-flex flex-column align-items-center pt-5">
        <Form.Group controlId="formGeneral">
          <Row className="mb-3">
            <Col sm={12} md={6} lg={5}>
              <Form.Label className="form-label">
                <User color="#3D3D3D" width="33" height="33" className="pr-2" />
                YOUR NAME
              </Form.Label>
            </Col>
            <Col sm={12} md={6} lg={7}>
              <Form.Control type="text" size="lg" />
            </Col>
          </Row>

          <Row className="mb-3">
            <Col sm={12} md={6} lg={5}>
              <Form.Label className="form-label">
                <Mail color="#3D3D3D" width="33" height="33" className="pr-2" />
                <span>EMAIL ADDRESS</span>
              </Form.Label>
            </Col>
            <Col sm={12} md={6} lg={7}>
              <Form.Control type="email" size="lg" />
            </Col>
          </Row>

          <Row className="mb-3">
            <Col sm={12} md={6} lg={5}>
              <Form.Label className="form-label">
                <MessageSquare color="#3D3D3D" width="33" height="33" className="pr-2" />
                YOUR MESSAGE
              </Form.Label>
            </Col>
            <Col sm={12} md={6} lg={7}>
              <Form.Control as="textarea" rows={5} size="lg" />
            </Col>
          </Row>

          <Button variant="dark" block className="btn-submit">
            SEND
          </Button>
        </Form.Group>
      </div>
    </Container>
  );
}
