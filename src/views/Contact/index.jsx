import React from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import { ChevronRight } from 'react-feather';
import { Col, Container, Row } from 'react-bootstrap';
import GeneralInquiries from './GeneralInquiries';
import FranchiseInquiries from './FranchiseInquiries';

function ContactRouter({ match }) {
  const { path } = match;

  const ContactPage = () => (
    <Container className="animating fade-in">
      <Row className="mt-5">
        <Col xl="12" className="contact-headline">
          <span>CONTACT</span>
        </Col>
      </Row>
      <Row className="contact-headline">
        <Col xl="12" className="text-center">
          <Link
            className="contact-headline d-flex align-items-center justify-content-center"
            to={`${path}/general-inquiries`}
          >
            <span className="mr-4">GENERAL INQUIRIES</span>
            <ChevronRight width="35" height="35" />
          </Link>
        </Col>
        <Col xl="12" className="text-center">
          <Link
            className="contact-headline d-flex align-items-center justify-content-center"
            to={`${path}/franchise-inquiries`}
          >
            <span className="mr-4">FRANCHISE INQUIRIES</span>
            <ChevronRight width="35" height="35" />
          </Link>
        </Col>
      </Row>
    </Container>
  );

  return (
    <>
      <Switch>
        <Route exact path={path} component={ContactPage} />
        <Route exact path={`${path}/general-inquiries`} component={GeneralInquiries} />
        <Route exact path={`${path}/franchise-inquiries`} component={FranchiseInquiries} />
      </Switch>
    </>
  );
}

export default ContactRouter;
