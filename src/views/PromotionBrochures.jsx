import React from 'react';
import { Container, Button } from 'react-bootstrap';

function PromotionBrochure() {
  return (
    <Container fluid className="promotion-brochure position-relative">
      <div className="promotion-brochure-headline-text">KEEP UP TO DATE WITH OUR LATEST PRODUCTS</div>
      <Button className="promotion-brochure-download-btn" variant="link">
        GET OUR PROMOTION!
      </Button>
    </Container>
  );
}

export default PromotionBrochure;
