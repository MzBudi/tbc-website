import React from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { Instagram, Facebook, Twitter } from 'react-feather';
import { SuspenseImage } from 'tbc';
import Carousel from 'react-multi-carousel';
import { socialMediaShared } from '../contents';
import SocialMediaFallback from './Fallbacks/SocialMediaFallback';
import 'react-multi-carousel/lib/styles.css';
import { SOCIAL_MEDIA_LINK } from '../constants';

class ImageList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imageState: null,
    };
  }

  async componentDidMount() {
    await this.loadImage();
  }

  loadImage = async () => {
    const { image } = this.props;
    try {
      if (image) {
        const result = await import(`../assets/img/pages/social-media-shared/${image}`);
        this.setState({
          imageState: result.default,
        });
      }
    } catch (error) {
      console.warn(error);
    }
  };

  render() {
    const { url } = this.props;
    const { imageState } = this.state;
    return (
      <a href={url} target="_blank" rel="noreferrer">
        <SuspenseImage fallback={SocialMediaFallback} src={imageState} alt={url} className="image" />
      </a>
    );
  }
}

function SocialMediaList() {
  return (
    <div className="social-media-shared-list">
      <div>
        <Button variant="link" className="mx-2 mx-md-4 item">
          <Facebook size={30} className="icon" />
        </Button>
        <Button variant="link" href={SOCIAL_MEDIA_LINK.IG} className="mx-1 mx-md-4 item">
          <Instagram size={30} className="icon" />
        </Button>
        <Button variant="link" className="mx-2 mx-md-4 item">
          <Twitter size={30} className="icon" />
        </Button>
      </div>
    </div>
  );
}

function SocialMediaSharedContents() {
  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 5,
      slidesToSlide: 1,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 3,
      slidesToSlide: 1,
      paritialVisibilityGutter: 30,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      slidesToSlide: 1,
      paritialVisibilityGutter: 30,
    },
  };

  return (
    <div className="d-flex flex-column justify-content-center w-100">
      <Row className="social-media-shared">
        <Col sm={12} className="my-4 d-flex align-items-center justify-content-center flex-column">
          <Button variant="link" href={SOCIAL_MEDIA_LINK.IG}>
            <Instagram size={36} className="icon" />
          </Button>
          <p className="text-center mt-4 social-media-shared-text">FOLLOW US</p>
          <p className="text-center social-media-shared-text">ON INSTAGRAM</p>
        </Col>
        <Col sm={12} className="carousel">
          <Carousel
            responsive={responsive}
            autoPlay
            autoPlaySpeed={5000}
            renderDotsOutside
            showDots
            infinite
            arrows={false}
            draggable
            swipeable
          >
            {socialMediaShared.map((item, idx) => (
              <ImageList {...item} key={idx} />
            ))}
          </Carousel>
        </Col>
        <Col sm={12}>
          <p className="text-center mt-4 pt-4 social-media-shared-text">FIND US ON</p>
          <SocialMediaList />
        </Col>
      </Row>
    </div>
  );
}

export default SocialMediaSharedContents;
