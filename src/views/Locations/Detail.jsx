import React, { useState } from 'react';
import classNames from 'classnames';
import { Redirect } from 'react-router-dom';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { SuspenseImage } from 'tbc';
import { navigateBack } from 'actions';
import { ChevronLeft } from 'react-feather';
import { storeLocationContents } from '../../contents';
import { StoreMainFallback, StoreMapFallback } from '../Fallbacks/StoreLocationFallback';
import DummyStore from '../../assets/img/dummy/xl-1200/store-main.png';
import manifest from '../../assets/img/dummy/xl-1200/manifest.json';
import { ReactComponent as Indonesia } from '../../assets/img/svg/indonesia.svg';

const { resolutions } = manifest;
const defSize = resolutions['store-map'];

function MapPreview({ lat, lon }) {
  const [isLoaded, setIsLoaded] = useState(false);

  const onLoaded = () => {
    setIsLoaded(true);
  };

  return (
    <div style={{ maxWidth: defSize[0], maxHeight: defSize[1] }}>
      <iframe
        title={`${lat},${lon}`}
        onLoad={onLoaded}
        className={classNames('pb-3 location-content-map', { 'd-none': !isLoaded })}
        src={`https://maps.google.com/maps?q=${lat}, ${lon}&z=15&output=embed`}
        frameBorder="0"
        allowFullScreen=""
        aria-hidden="false"
      />
      <StoreMapFallback className={classNames('pb-3', { 'd-none': isLoaded })} />
    </div>
  );
}
class LocationDetail extends React.Component {
  constructor(props) {
    super(props);

    const {
      match: {
        params: { locationKey },
      },
    } = props;
    const locationContent = storeLocationContents[locationKey];

    this.state = {
      storeImage: DummyStore,
      locationContent,
    };
  }

  async componentDidMount() {
    const { locationContent } = this.state;
    if (!locationContent) return;

    const { image } = locationContent;
    const loadedImage = await this.loadImage(image);
    this.setState({ storeImage: loadedImage });
  }

  loadImage = async (image) => {
    try {
      if (image) {
        const result = await import(`../../assets/img/pages/store/${image}`);
        return result.default;
      }
    } catch (error) {
      console.warn(error);
      return DummyStore;
    }
  };

  render() {
    const { storeImage, locationContent } = this.state;
    if (!locationContent) return <Redirect to="/not-found" />;

    const { name, description, addressLine, openHours, location } = locationContent;
    return (
      <Container className="my-5">
        <Row>
          <Col sm={12} md={7} lg={7} className="animating fade-in-right">
            <div className="location-content-image-section">
              <SuspenseImage src={storeImage} fallback={StoreMainFallback} alt={name} className="image" />
            </div>
          </Col>
          <Col sm={12} md={4} lg={4} className="mt-4 mt-sm-0 mt-sm-0 mt-md-0 animating fade-in-left">
            <h2 className="def-font-family location-content-header-text">Shop Data</h2>
            <hr />
            <h3 className="def-font-family location-content-header-text">Address</h3>
            <p className="h5 location-content-address-detail def-font-family">{addressLine}</p>
            <br />

            <MapPreview lat={location.lat} lon={location.lon} />

            <hr />

            <h4 className="def-font-family location-content-header-text"> Open Hours :</h4>
            <p className="location-content-address-detail def-font-family">
              {openHours.hours}
              <br />
              {openHours.frequency}
            </p>
          </Col>
        </Row>
        <Row>
          <Col sm={12} md={12} lg={12}>
            <Button variant="outline-dark" className="mt-4 back-button" onClick={navigateBack}>
              <ChevronLeft />
              <Indonesia style={{ color: 'white' }} className="mx-3" />
              Back to locations
            </Button>
          </Col>
          <Col
            xs={{ span: 12, order: 2 }}
            sm={{ span: 12, order: 2 }}
            md={{ span: 12, order: 3 }}
            lg={{ span: 12, order: 3 }}
          >
            <h2 className="locations-description my-4">
              <span className="def-font-family font-weight-bold">The Breakfast Club | </span>
              <span className="font-size-large def-font-family font-italic font-weight-lighter"> {name}</span>
            </h2>
            <p className="def-font-family location-content-address-detail text-justify">{description}</p>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default LocationDetail;
