import React from 'react';
import classNames from 'classnames';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { SuspenseImage } from 'tbc';
import { storeLocationContents } from '../../contents';
import { StorePreviewFallback } from '../Fallbacks/StoreLocationFallback';

class LocationList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imageState: null,
    };
  }

  async componentDidMount() {
    await this.loadImage();
  }

  loadImage = async () => {
    const { image } = this.props;
    try {
      if (image) {
        const result = await import(`../../assets/img/pages/store/${image}`);
        this.setState({
          imageState: result.default,
        });
      }
    } catch (error) {
      console.warn(error);
    }
  };

  render() {
    const { name, path, locationKey } = this.props;
    const { imageState } = this.state;
    return (
      <div className="item">
        <Link to={`${path}/${locationKey}`} className="position-relative">
          <SuspenseImage
            src={imageState}
            fallback={StorePreviewFallback}
            alt="thebreakfastclub.co.id"
            className="image"
            fallbackClass="img-fluid"
          />
        </Link>
        <h2 className="title">{name}</h2>
      </div>
    );
  }
}

function LocationPreview({ match }) {
  const { path } = match;
  return (
    <Container className="location-preview">
      <h1 className="title">LOCATIONS</h1>
      <Row className="mt-4">
        {Object.values(storeLocationContents).map((item, idx) => (
          <Col
            key={idx}
            lg={4}
            sm={12}
            md={12}
            className={classNames('animating', {
              'fade-in-right': idx === 0,
              'fade-in-left': idx % 2 === 0,
              'fade-in-down': idx % 1 === 0,
            })}
          >
            <LocationList {...item} key={item.key} locationKey={item.key} path={path} />
          </Col>
        ))}
      </Row>
    </Container>
  );
}

export default LocationPreview;
