import React, { lazy } from 'react';
import { Switch, Route } from 'react-router-dom';

const LocationPreview = lazy(() => import('./Preview'));
const LocationDetail = lazy(() => import('./Detail'));

function LocationsRouter({ match }) {
  const { path } = match;
  return (
    <>
      <Switch>
        <Route exact path={`${path}`} component={LocationPreview} />
        <Route path={`${path}/:locationKey`} component={LocationDetail} />
      </Switch>
    </>
  );
}

export default LocationsRouter;
