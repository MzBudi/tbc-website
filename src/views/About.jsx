import React from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import { SuspenseJumbotron } from 'tbc';
import aboutHeaderImage from '../assets/img/pages/about-us-header.webp';

function About() {
  return (
    <>
      <SuspenseJumbotron srcImage={aboutHeaderImage} alt="The Breakfast Club - About" className="page-jumbotron">
        <div className="h-100 d-flex justify-content-center align-items-center">
          <h1 className="header-headline header-title">About Us</h1>
        </div>
      </SuspenseJumbotron>

      <Row className="my-5">
        <Col sm="12" md="12" lg="12">
          <Container className="text-justify font-size-large py-4 def-font-family def-font-color">
            <p className="about-us-content">
              The Breakfast Club mengadopsi konsep fresh to cup yang menyajikan produk berbahan dasar susu dengan
              berbagai pilihan rasa yang unik dan menarik serta harga yang terjangkau. Pemilihan bahan baku terbaik di
              setiap menunya dan proses produksi yang efektif adalah prinsip kami. Penyajian yang modern dan higenis,
              serta selalu mengedepankan cita rasa dan menjaga kualitas mutu adalah tujuan kami. Hal tersebut membuat
              produk kami begitu di cintai masyarakat dan terus berkembang seiring waktu.
            </p>
            <p className="about-us-content">
              Visi The Breakfast Club adalah meningkatkan brand awareness, menguatkan pondasi brand position sebagai
              pioneer produk berbahan dasar susu dengan menu yang belum pernah ada sebelumnya, serta menjadi pelopor
              gerakan minum susu untuk meningkatkan kualitas kesehatan anak bangsa. Kedepannya kami akan mengembangkan
              visi dan misi tersebut melalui konsep waralaba. Kami berharap The Breakfast Club dapat ikut andil
              menciptakan lapangan kerja dan menjadi platform untuk anak bangsa berkarya.
            </p>
          </Container>
        </Col>
      </Row>
    </>
  );
}

export default About;
