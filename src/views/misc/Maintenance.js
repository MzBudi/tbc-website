import React from 'react';

import backgroundTbc from '../../assets/img/pages/background-tbc.webp';

class Maintenance extends React.Component {
  render() {
    return (
      <div className="d-flex flex-column-reverse justify-content-center justify-content-xl-between h-100 w-100 pl-3 overflow-auto align-items-stretch">
        <div>
          <img src={backgroundTbc} alt="underMaintenance" className="img-fluid mb-1 w-auto pr-5" />
          <p className="font-size-large pt-4 line-height-1">Get relax! We are brewing something good!</p>
        </div>
      </div>
    );
  }
}
export default Maintenance;
