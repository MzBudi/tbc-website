import React from 'react';
import { navigateBack } from 'actions';
import { Button } from 'react-bootstrap';
import errorImg from '../../../assets/img/pages/404.webp';

class Error404 extends React.Component {
  render() {
    return (
      <div className="d-flex flex-column-reverse justify-content-center align-items-center h-100">
        <div className="text-center">
          <img src={errorImg} alt="ErrorImg" className="img-fluid align-self-center" />
          <h1 className="font-large-2 my-1">404 - Page Not Found!</h1>
          <p className="pt-2 mb-0">Oops!</p>
          <p className="pb-2">Sorry! Your requested stuff has been moved</p>
          <Button onClick={navigateBack} variant="outline-dark" className="back-button">
            Return back
          </Button>
        </div>
      </div>
    );
  }
}

export default Error404;
