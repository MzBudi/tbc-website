import React from 'react';
import { Nav } from 'react-bootstrap';

function Footer() {
  return (
    <div className="footer mt-4">
      <Nav className="justify-content-center footer-link-text">
        <Nav.Item className="ml-4 mr-4 mobile-footer">
          <Nav.Link eventKey={1} href="#about" className="footer-nav-link-text">
            ABOUT US
          </Nav.Link>
        </Nav.Item>
        <Nav.Item className="ml-4 mr-4 mobile-footer">
          <Nav.Link eventKey={2} href="#products" className="footer-nav-link-text">
            PRODUCTS
          </Nav.Link>
        </Nav.Item>
        <Nav.Item className="ml-4 mr-4 mobile-footer">
          <Nav.Link eventKey={3} href="#blogs" className="footer-nav-link-text">
            BLOGS
          </Nav.Link>
        </Nav.Item>
        <Nav.Item className="ml-4 mr-4 mobile-footer">
          <Nav.Link eventKey={4} href="#locations" className="footer-nav-link-text">
            LOCATIONS
          </Nav.Link>
        </Nav.Item>
        <Nav.Item className="ml-4 mr-4 mobile-footer">
          <Nav.Link eventKey={5} href="#contact" className="footer-nav-link-text">
            CONTACT
          </Nav.Link>
        </Nav.Item>
      </Nav>
      <p className="text-center mt-4 footer-copyright pb-4">&copy; 2022 HAK CIPTA TERPELIHARA PT THE BREAKFAST CLUB</p>
    </div>
  );
}

export default Footer;
