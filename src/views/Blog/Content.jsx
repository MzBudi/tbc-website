import React, { useState, useEffect } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { SuspenseImage, SuspenseJumbotron } from 'tbc';
import { Button, Col, Container, Row } from 'react-bootstrap';
import { ChevronLeft } from 'react-feather';
import BlogContentFallback from '../Fallbacks/BlogContentFallback';
import blogContentHeaderDummy from '../../assets/img/dummy/xl-1200/blog-content-header.png';
import { blogContents } from '../../contents';
import { BLOG_CONTENT_TYPE } from '../../constants';

export default function BlogContent({ match }) {
  const [headerImage, setHeaderImage] = useState(blogContentHeaderDummy);
  const {
    params: { blogKey },
  } = match;

  const blogContent = blogContents[blogKey];
  const { header, contents } = blogContent !== undefined ? blogContent : {};

  const loadBlogContentImage = async (previewImage) => {
    try {
      const result = await import(`../../assets/img/pages/blog-contents/${previewImage}`);
      return result.default;
    } catch (err) {
      console.warn(`Cannot find image: ${previewImage}`);
    }
  };

  const ImageContent = ({ imageSource }) => {
    const [image, setImage] = useState(undefined);

    useEffect(() => {
      let render = true;
      if (render) {
        loadBlogContentImage(imageSource).then((loadedImage) => setImage(loadedImage || undefined));
      }

      return () => {
        render = true;
      };
    }, []);

    return <SuspenseImage src={image} fallback={BlogContentFallback} className="blog-contents-image" />;
  };

  const TextContent = ({ text, style }) => (
    <span className="blog-contents-text" style={style}>
      {text}
    </span>
  );

  useEffect(() => {
    let render = true;
    if (render && header) {
      const { previewImage } = header;
      loadBlogContentImage(previewImage).then((loadedImage) => setHeaderImage(loadedImage || headerImage));
    }

    return () => {
      render = false;
    };
  }, []);

  if (!blogContent) return <Redirect to="/not-found" />;
  return (
    <>
      <SuspenseJumbotron srcImage={headerImage} alt={header.title.text}>
        <div className="h-100 d-flex justify-content-center align-items-center">
          <h1 className="header-title" style={header.title.style}>
            {header.title.text}
          </h1>
        </div>
      </SuspenseJumbotron>

      <Row className="my-5">
        <Container fluid>
          <Col sm="12" md="12" lg="12">
            <Link to="/blogs">
              <Button variant="outline-dark" className="back-button">
                <ChevronLeft />
                Back to Blogs
              </Button>
            </Link>
          </Col>

          <Col sm="12" md="12" lg="12" className="d-flex justify-content-center">
            <div className="blog-contents">
              {contents.map((item, idx) => {
                const { type, content, style } = item;
                switch (type) {
                  case BLOG_CONTENT_TYPE.IMAGE:
                    return <ImageContent key={idx} imageSource={content} />;
                  case BLOG_CONTENT_TYPE.TEXT:
                    return <TextContent key={idx} text={content} style={style} />;
                  default:
                    return <div key={idx} />;
                }
              })}
            </div>
          </Col>
        </Container>
      </Row>
    </>
  );
}
