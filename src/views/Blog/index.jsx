import React, { lazy } from 'react';
import { Switch, Route } from 'react-router-dom';

const BlogPreview = lazy(() => import('./Preview'));
const BlogContent = lazy(() => import('./Content'));

export default function BlogRouter({ match }) {
  const { path } = match;
  return (
    <Switch>
      <Route exact path={path} component={BlogPreview} />
      <Route exact path={`${path}/:blogKey`} component={BlogContent} />
    </Switch>
  );
}
