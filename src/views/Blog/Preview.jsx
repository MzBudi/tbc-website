import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { ArrowRight, Plus } from 'react-feather';
import { SuspenseJumbotron, SuspenseTile } from 'tbc';
import { Col, Row, Button } from 'react-bootstrap';
import blogHeaderImage from '../../assets/img/pages/blog-header.webp';
import { blogContentsTiles } from '../../contents';

const BlogTile = ({ path, animate, ...props }) => {
  const [image, setImage] = useState(undefined);
  const { blogKey, title, subtitle, previewImage } = props;

  const loadImage = async () => {
    try {
      if (previewImage) {
        const result = await import(`../../assets/img/pages/blog-contents/${previewImage}`);
        setImage(result.default);
      }
    } catch (err) {
      console.warn(err);
    }
  };

  useEffect(() => {
    let mounted = true;
    if (mounted) {
      loadImage().then();
    }
    return () => {
      mounted = false;
    };
  }, [previewImage]);

  return (
    <Col lg={3} md={6} sm={12} className={`blog-tile animating ${animate}`}>
      <SuspenseTile srcImage={image} className="blog-tile-image" alt={title.text}>
        <div className="blog-tile-content">
          <div className="blog-tile-label">
            <div className="blog-tile-content-title" style={title.style}>
              {title.text}
            </div>
            <div className="blog-tile-content-subtitle" style={subtitle.style}>
              {subtitle.text}
            </div>
            <div className="d-flex flex-row-reverse">
              <Link to={`${path}/${blogKey}`}>
                <ArrowRight className="def-font-color" />
              </Link>
            </div>
          </div>
        </div>
      </SuspenseTile>
    </Col>
  );
};

const BlogTiles = ({ path, tiles = [] }) =>
  tiles.map((props, idx) => (
    <BlogTile
      key={props.blogKey + idx}
      {...props}
      path={path}
      animate={idx % 2 === 0 ? 'fade-in-up' : 'fade-in-down'}
    />
  ));

export default function BlogPreview({ match }) {
  const maxCount = blogContentsTiles.length;
  const countStep = 4;
  const [first, setFirst] = useState(true);
  const [tilesToShow, setTilesToShow] = useState([]);
  const { path } = match;

  const onLoadMore = () => {
    const currCount = tilesToShow.length;
    const _tilesToShow = blogContentsTiles.slice(0, currCount + countStep);
    setTilesToShow(_tilesToShow);
  };

  useEffect(() => {
    let render = true;
    if (render && first) {
      onLoadMore();
      setFirst(false);
    }

    return () => {
      render = false;
    };
  }, []);

  return (
    <>
      <SuspenseJumbotron srcImage={blogHeaderImage} alt="The Breakfast Club - Blogs" className="page-jumbotron">
        <div className="h-100 d-flex justify-content-center align-items-center">
          <h1 className="header-headline header-title">We want you to know...</h1>
        </div>
      </SuspenseJumbotron>
      <Row className="justify-content-center">
        <Col sm={11} md={11} lg={11} className="py-5 mx-3 blog-brief">
          Aliquam lacinia aliquet quam ut rhoncus. Vestibulum id diam porta, auctor velit eu, pretium nibh. Quisque non
          nisl dignissim, consectetur enim at, egestas tortor. In facilisis, enim vel vehicula vehicula, nisl sem varius
          diam, quis aliquet elit augue eu elit. Curabitur mollis augue sapien, in vehicula risus congue et.
        </Col>

        <Col sm={12} mg={12} lg={12}>
          <Row className="m-0">
            <BlogTiles path={path} tiles={tilesToShow} />
          </Row>
        </Col>
        <Row className="w-100 d-flex justify-content-center pb-4">
          <Button
            className="load-more-btn"
            variant="light"
            onClick={onLoadMore}
            disabled={tilesToShow.length >= maxCount}
          >
            <span>Load More</span>
            <Plus />
          </Button>
        </Row>
      </Row>
    </>
  );
}
