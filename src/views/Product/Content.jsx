import React, { useState, useEffect } from 'react';
import classNames from 'classnames';
import { Link, Redirect } from 'react-router-dom';
import { SuspenseImage } from 'tbc';
import { Container, Col, Row, Button, Card } from 'react-bootstrap';
import Carousel from 'react-multi-carousel';
import { ChevronLeft } from 'react-feather';
import ProductPreviewFallback from '../Fallbacks/ProductPreviewFallback';
import productContentDummy from '../../assets/img/dummy/xl-1200/product-preview.png';
import { productContents, productContentsTiles } from '../../contents';

const loadProductContentImage = async (previewImage) => {
  try {
    const result = await import(`../../assets/img/pages/product-contents/${previewImage}`);
    return result.default;
  } catch (err) {
    return productContentDummy;
  }
};

const ImageContent = ({ imageSource, className }) => {
  const [image, setImage] = useState(undefined);

  useEffect(() => {
    let render = true;
    if (render) {
      loadProductContentImage(imageSource).then((loadedImage) => setImage(loadedImage));
    }

    return () => {
      render = true;
    };
  }, [imageSource]);
  return (
    <SuspenseImage
      src={image}
      fallback={ProductPreviewFallback}
      alt="thebreakfastclub.co.id"
      className={className}
      fallbackClass="img-fluid"
    />
  );
};

export default function ProductContent({ match }) {
  const {
    params: { productKey, categoryKey },
  } = match;

  const productContent = productContents[productKey];
  const relatedProducts = productContentsTiles[categoryKey] || [];

  if (!productContent) return <Redirect to="/not-found" />;

  const ProductImages = ({ images = [] }) => {
    const [mainImage, setMainImage] = useState(images[0]);
    const [fadeInDown, setFadeInDown] = useState(false);

    const changeImageContentPreview = (index) => {
      setFadeInDown(false);
      setMainImage(images[index]);
      setTimeout(() => {
        setFadeInDown(true);
      }, 1);
    };

    return (
      <div className="preview">
        <Row className="image-container">
          <Col
            sm={12}
            md={12}
            lg={12}
            className={classNames('main-image-col', { animating: true, 'fade-in-down': fadeInDown })}
          >
            <ImageContent imageSource={mainImage} className="main-image" />
          </Col>
          {images.slice(0, 3).map((src, idx) => (
            <Col lg={4} sm={4} md={4} xs={4} xl={4} className="image-col">
              <Card.Body
                key={idx}
                onClick={() => changeImageContentPreview(idx)}
                className="animating fade-in-left p-0"
              >
                <ImageContent imageSource={src} className="img-fluid" />
              </Card.Body>
            </Col>
          ))}
        </Row>
      </div>
    );
  };

  const ProductContentPreview = () => (
    <Row className="product-content">
      <Col lg="4" md="5" sm="12" className="animating fade-in-right">
        <ProductImages images={productContent.images || []} />
      </Col>
      <Col lg="8" md="7" sm="12" className="detail animating fade-in-left">
        <h2 className="name-price">{productContent.name}</h2>
        <h3 className="name-price">{productContent.price}</h3>
        <hr />
        <p className="description">{productContent.description}</p>
        <hr />
        {Object.keys(productContent.specification).map((key) => (
          <Row className="description w-100">
            <Col sm={4} md={4} lg={4}>
              <span className="font-weight-bold" style={{ fontSize: '1.5rem' }}>
                {key}:
              </span>
            </Col>
            <Col sm={8} md={8} lg={8}>
              <ul>
                {Array.isArray(productContent.specification[key]) ? (
                  productContent.specification[key].map((spec) => <li style={{ fontSize: '1.4rem' }}>{spec}</li>)
                ) : (
                  <div style={{ fontSize: '1.4rem' }}>{productContent.specification[key]}</div>
                )}
              </ul>
            </Col>
          </Row>
        ))}
      </Col>
    </Row>
  );

  const RelatedProductPreview = () => {
    const relateResponsive = {
      desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3,
        slidesToSlide: 1,
      },
      tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2,
        slidesToSlide: 1,
      },
      mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        slidesToSlide: 1,
      },
    };

    return (
      <>
        <div className="my-4 relate-product-headline">RELATED PRODUCTS</div>
        <div className="mb-4 product-content">
          <Carousel
            responsive={relateResponsive}
            additionalTransfrom={0}
            arrows={false}
            autoPlaySpeed={3000}
            draggable
            focusOnSelect={false}
            infinite
            keyBoardControl
            minimumTouchDrag={80}
            renderButtonGroupOutside
            showDots
            dotListClass="custom-dot-list-style"
            renderDotsOutside={false}
            containerClass="carousel-container"
          >
            {relatedProducts.map((item, idx) => {
              const { previewImage } = item;
              const { name, price } = productContents[item.productKey];
              if (productKey === item.productKey) return null;
              return (
                <div key={idx} className="px-0 px-md-3 animating fade-in-right">
                  <Link to={`${item.productKey}`} className="position-relative">
                    <ImageContent imageSource={previewImage} className="img-fluid" />
                    <div className="relate-product-item-text">
                      <div className="relate-product-item-name">{name}</div>
                      <div className="relate-product-item-price">{price}</div>
                    </div>
                  </Link>
                </div>
              );
            })}
          </Carousel>
        </div>
      </>
    );
  };

  return (
    <Container className="mt-5">
      <ProductContentPreview />

      <div className="mt-4">
        <Link to="/products">
          <Button variant="outline-dark" className="back-button">
            <ChevronLeft />
            Back to Products
          </Button>
        </Link>
      </div>
      <RelatedProductPreview />
    </Container>
  );
}
