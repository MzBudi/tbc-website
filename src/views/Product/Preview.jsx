import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { SuspenseImage, SuspenseJumbotron } from 'tbc';
import { Nav, Container } from 'react-bootstrap';
import Carousel from 'react-multi-carousel';
import productHeaderImage from '../../assets/img/pages/product-header.webp';
import ProductPreviewFallback from '../Fallbacks/ProductPreviewFallback';
import { productCategories, productContentsTiles, productContents } from '../../contents';
import 'react-multi-carousel/lib/styles.css';

const ProductCategories = () =>
  productCategories.map((item, index) => (
    <Nav.Link key={index} eventKey={item.key}>
      {item.text}
    </Nav.Link>
  ));

const ProductTile = ({ path, productKey, previewImage, categoryKey }) => {
  const [image, setImage] = useState(null);
  const productContent = productContents[productKey];
  const { name, price } = productContent || {};

  const loadImage = async () => {
    try {
      if (previewImage) {
        const result = await import(`../../assets/img/pages/product-contents/${previewImage}`);
        setImage(result.default);
      }
    } catch (err) {
      console.warn(err);
    }
  };

  useEffect(() => {
    let mounted = true;
    if (mounted) {
      loadImage().then();
    }
    return () => {
      mounted = false;
    };
  }, [previewImage]);

  if (!productContent) return null;
  return (
    <div className="animating fade-in item">
      <Link to={`${path}/${categoryKey}/${productKey}`} className="position-relative">
        <SuspenseImage
          src={image}
          fallback={ProductPreviewFallback}
          alt="thebreakfastclub.co.id"
          className="image"
          fallbackClass="img-fluid-h"
        />
      </Link>
      <div className="text">
        <p className="name">{name}</p>
        <p className="price">{price}</p>
      </div>
    </div>
  );
};

const ProductTiles = ({ path, tiles = [] }) => {
  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 4,
      slidesToSlide: 1,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
      slidesToSlide: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      slidesToSlide: 1,
    },
  };

  if (!tiles.length)
    return (
      <div className="animating fade-in font-size-large text-center">
        Sorry, currently we are on indexing this category
      </div>
    );

  return (
    <Carousel
      responsive={responsive}
      additionalTransfrom={0}
      arrows={false}
      autoPlay
      autoPlaySpeed={3000}
      draggable
      focusOnSelect={false}
      infinite
      keyBoardControl
      showDots
      minimumTouchDrag={80}
      renderDotsOutside
      dotListClass="custom-carousel-dot-list"
    >
      {tiles.map((props, index) => (
        <ProductTile key={props.productKey + index} path={path} {...props} />
      ))}
    </Carousel>
  );
};

export default function ProductPreview({ match }) {
  const defaultActiveKey = productCategories[0].key;
  const [filterTiles, setShowFilterTiles] = useState([]);
  const { path } = match;

  const showTiles = (eventKey) => {
    const tiles = productContentsTiles[eventKey];
    setShowFilterTiles(tiles);
  };

  useEffect(() => {
    let mounted = true;
    if (mounted) {
      showTiles(defaultActiveKey);
    }
    return () => {
      mounted = false;
    };
  }, []);

  return (
    <>
      <SuspenseJumbotron srcImage={productHeaderImage} alt="The Breakfast Club - Products" className="page-jumbotron">
        <div className="h-100 d-flex justify-content-center align-items-center">
          <h1 className="header-headline header-title">Our Products...</h1>
        </div>
      </SuspenseJumbotron>
      <Container className="my-5">
        <Nav justify className="product-category-pills" defaultActiveKey={defaultActiveKey} onSelect={showTiles}>
          <Nav.Item>
            <ProductCategories />
          </Nav.Item>
        </Nav>
        <div className="product-preview">
          <ProductTiles path={path} tiles={filterTiles} />
        </div>
      </Container>
    </>
  );
}
