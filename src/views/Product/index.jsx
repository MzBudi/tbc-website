import React, { lazy } from 'react';
import { Switch, Route } from 'react-router-dom';

const Product = lazy(() => import('./Preview'));
const ProductContent = lazy(() => import('./Content'));

function ProductRouter({ match }) {
  const { path } = match;
  return (
    <Switch>
      <Route exact path={`${path}`} component={Product} />
      <Route exact path={`${path}/:categoryKey/:productKey`} component={ProductContent} />
    </Switch>
  );
}

export default ProductRouter;
