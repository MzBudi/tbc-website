import React, { useState, useEffect } from 'react';
import { Col, Row, Button, Container } from 'react-bootstrap';
import { SuspenseImage, SuspenseJumbotron, SuspenseTile } from 'tbc';
import { ArrowRight, ChevronLeft, ChevronRight } from 'react-feather';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import Carousel from 'react-multi-carousel';

import HomeHeaderFallback from './Fallbacks/HomeHeaderFallback';
import { blogContentsTiles, homeHeader } from '../contents';
import Merchandise from './Merchandise';
import { useWindowDimensions } from '../utility/utilities';
import { HOME_HEADER_SOURCE, ROUTES_ROOT_PATH } from '../constants';
import { merchandisesSectionRef } from '../refs';

import 'react-multi-carousel/lib/styles.css';

function HomeTile({ animate, ...props }) {
  const [img, setImg] = useState(undefined);
  const { title, subtitle, previewImage, path, blogKey } = props;

  const imgLoader = async () => {
    try {
      if (previewImage) {
        const result = await import(`../assets/img/pages/blog-contents/${previewImage}`);
        setImg(result.default);
      }
    } catch (error) {
      console.warn(error);
    }
  };

  useEffect(() => {
    let mounted = true;
    if (mounted) {
      imgLoader().then();
    }

    return () => {
      mounted = false;
    };
  });

  return (
    <Col lg={3} md={6} sm={12} className={`blog-tile animating ${animate}`}>
      <SuspenseTile srcImage={img} alt={title.text}>
        <div className="blog-tile-content">
          <div className="blog-tile-label">
            <div className="blog-tile-content-title" style={title.style}>
              {title.text}
            </div>
            <div className="blog-tile-content-subtitle" style={subtitle.style}>
              {subtitle.text}
            </div>
            <div className="d-flex flex-row-reverse">
              <Link to={`${path}blogs/${blogKey}`}>
                <ArrowRight className="def-font-color" />
              </Link>
            </div>
          </div>
        </div>
      </SuspenseTile>
    </Col>
  );
}

const CarouselArrowButton = ({ carouseState, next, previous }) => {
  const { currentSlide, slidesToShow, totalItems } = carouseState;
  const { width } = useWindowDimensions();

  return (
    <div className={classNames('carousel-arrow-button-group', { 'd-none': width < 1024 })}>
      <Button className="arrow-button" onClick={previous} disabled={currentSlide === 0}>
        <ChevronLeft size={30} className="arrow" />
      </Button>
      <Button className="arrow-button" onClick={next} disabled={slidesToShow === totalItems}>
        <ChevronRight size={30} className="arrow" />
      </Button>
    </div>
  );
};

const CustomDot = ({ index, goToSlide, active }) => (
  <li
    data-index={index}
    className={classNames('react-multi-carousel-dot', { 'react-multi-carousel-dot--active': active })}
  >
    <button onClick={() => goToSlide(index + 2)} type="button" />
  </li>
);

const CarouselButtonGroup = ({ carouselState, goToSlide, ...rest }) => {
  const { totalItems, currentSlide } = carouselState;

  return (
    <div className="carousel-button-group">
      <Container>
        <ul className="react-multi-carousel-dot-list">
          {Array.from(Array(parseInt(totalItems / 2, 10)).keys()).map((_, idx) => (
            <CustomDot key={idx} index={idx} goToSlide={goToSlide} active={currentSlide === idx + 2} />
          ))}
        </ul>
        <CarouselArrowButton carouseState={carouselState} goToSlide={goToSlide} {...rest} />
      </Container>
    </div>
  );
};

class HomeHeader extends React.Component {
  constructor(props) {
    super(props);

    const { source, previewImage: imageFile, headerKey, title, subtitle, metadata } = this.props;

    let assetRoot;
    switch (source) {
      case HOME_HEADER_SOURCE.BLOGS:
        assetRoot = 'blog-contents/';
        break;
      case HOME_HEADER_SOURCE.PRODUCTS:
        assetRoot = 'product-contents/';
        break;
      case HOME_HEADER_SOURCE.MERCHANDISES:
        assetRoot = 'merchandises/';
        break;
      case HOME_HEADER_SOURCE.LOCATIONS:
        assetRoot = 'store/';
        break;
      default:
        assetRoot = '';
        break;
    }

    this.state = {
      headerKey,
      source,
      image: undefined,
      assetRoot,
      imageFile,
      title,
      subtitle,
      metadata: metadata || {},
    };
  }

  async componentDidMount() {
    await this.loadImage();
  }

  loadImage = async () => {
    const { assetRoot, imageFile } = this.state;
    try {
      if (imageFile) {
        const img = await import(`../assets/img/pages/${assetRoot}${imageFile}`);
        this.setState({
          image: img.default,
        });
      }
    } catch (error) {
      console.warn(error.message);
    }
  };

  renderBlog = () => {
    const { image, headerKey, title, subtitle } = this.state;
    return (
      <SuspenseJumbotron className="header" srcImage={image} alt={`The Breakfast Club - ${title || 'Blogs'}`}>
        <div className="headline">
          <Link to={`${ROUTES_ROOT_PATH.BLOGS}/${headerKey}`}>
            <h1 className="title">{title}</h1>
            <h2 className="subtitle">{subtitle}</h2>
            <h3>
              <ArrowRight size={30} className="arrow-button" />
            </h3>
          </Link>
        </div>
      </SuspenseJumbotron>
    );
  };

  renderProduct = () => {
    const { image, headerKey, title, subtitle, metadata } = this.state;
    return (
      <SuspenseJumbotron className="header" srcImage={image} alt={`The Breakfast Club - ${title || 'Products'}`}>
        <div className="headline">
          <Link to={`${ROUTES_ROOT_PATH.PRODUCTS}/${metadata.categoryKey}/${headerKey}`}>
            <h1 className="title">{title}</h1>
            <h2 className="subtitle">{subtitle}</h2>
            <h3>
              <ArrowRight size={30} className="arrow-button" />
            </h3>
          </Link>
        </div>
      </SuspenseJumbotron>
    );
  };

  renderMerchandise = () => {
    const { image, headerKey, title, subtitle } = this.state;
    return (
      <SuspenseJumbotron className="header" srcImage={image} alt={`The Breakfast Club - ${title || 'Merchandises'}`}>
        <div className="headline">
          <Link
            to={`/?merchandises=${headerKey}`}
            onClick={() => merchandisesSectionRef.current.scrollIntoView({ behavior: 'smooth' })}
          >
            <h1 className="title">{title}</h1>
            <h2 className="subtitle">{subtitle}</h2>
            <h3>
              <ArrowRight size={30} className="arrow-button" />
            </h3>
          </Link>
        </div>
      </SuspenseJumbotron>
    );
  };

  renderLocation = () => {
    const { image, headerKey, title, subtitle } = this.state;
    return (
      <SuspenseJumbotron srcImage={image} className="header" alt={`The Breakfast Club - ${title || 'Locations'}`}>
        <div className="headline">
          <Link to={`${ROUTES_ROOT_PATH.LOCATIONS}/${headerKey}`}>
            <h1 className="title">{title}</h1>
            <h2 className="subtitle">{subtitle}</h2>
            <h3>
              <ArrowRight size={30} className="arrow-button" />
            </h3>
          </Link>
        </div>
      </SuspenseJumbotron>
    );
  };

  render() {
    const { image, imageFile, source } = this.state;

    switch (source) {
      case HOME_HEADER_SOURCE.BLOGS:
        return this.renderBlog();
      case HOME_HEADER_SOURCE.PRODUCTS:
        return this.renderProduct();
      case HOME_HEADER_SOURCE.MERCHANDISES:
        return this.renderMerchandise();
      case HOME_HEADER_SOURCE.LOCATIONS:
        return this.renderLocation();
      default:
        return (
          <div>
            <SuspenseImage
              src={image}
              alt={imageFile || source || 'header'}
              fallback={HomeHeaderFallback}
              className="image-slide img-fluid"
            />
          </div>
        );
    }
  }
}

function Home({ match }) {
  const { path } = match;
  const { width } = useWindowDimensions();

  const sortNewest = (a, b) => {
    if (a.publishTime < b.publishTime) {
      return -1;
    }
    if (a.publishTime > b.publishTime) {
      return 1;
    }
    return 0;
  };

  const responsive = {
    desktopNarrow: {
      breakpoint: { max: 3000, min: 1366 },
      items: 1,
      slidesToSlide: 1,
    },
    desktop: {
      breakpoint: { max: 1366, min: 1024 },
      items: 1,
      slidesToSlide: 1,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
      slidesToSlide: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      slidesToSlide: 1,
    },
  };

  const TilesLoader = () => {
    const newestContent = blogContentsTiles.sort(sortNewest);

    const contentTiles = newestContent.slice(0, 8);
    return contentTiles.map((props, idx) => (
      <HomeTile
        path={path}
        key={props.blogKey + idx}
        {...props}
        animate={idx % 2 === 0 ? 'fade-in-up' : 'fade-in-down'}
      />
    ));
  };

  return (
    <div className="home">
      <div className="carousel">
        <Carousel
          responsive={responsive}
          arrows={false}
          infinite
          autoPlaySpeed={5000}
          draggable={width <= 1024}
          focusOnSelect={false}
          keyBoardControl
          minimumTouchDrag={40}
          renderButtonGroupOutside
          customButtonGroup={<CarouselButtonGroup />}
        >
          {homeHeader.map((item) => (
            <HomeHeader key={item.headerKey} {...item} />
          ))}
        </Carousel>
      </div>
      <Row className="my-5">
        <Col sm={12} md={12} lg={12}>
          <Container fluid className="intro-section">
            The Breakfast Club mengadopsi konsep fresh to cup yang menyajikan produk berbahan dasar susu dengan berbagai
            pilihan rasa yang unik dan menarik serta harga yang terjangkau. Pemilihan bahan baku terbaik di setiap
            menunya dan proses produksi yang efektif adalah prinsip kami. Penyajian yang modern dan higenis, serta
            selalu mengedepankan cita rasa dan menjaga kualitas mutu adalah tujuan kami. Hal tersebut membuat produk
            kami begitu di cintai masyarakat dan terus berkembang seiring waktu.
          </Container>
        </Col>
        <Col sm={12} md={12} lg={12} className="blog-section">
          <Row className="m-0 justify-content-center">
            <TilesLoader />
          </Row>
        </Col>
      </Row>
      <Merchandise />
    </div>
  );
}

export default Home;
