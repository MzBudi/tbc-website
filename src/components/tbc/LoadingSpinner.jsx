import React from 'react';
import { Spinner } from 'react-bootstrap';

function LoadingSpinner() {
  return (
    <div className="fallback-spinner">
      <Spinner animation="border" variant="dark" />
    </div>
  );
}

export default LoadingSpinner;
