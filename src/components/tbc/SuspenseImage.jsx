import React, { Suspense } from 'react';

const imgCache = {
  __cache: {},
  read(src) {
    if (!this.__cache[src]) {
      this.__cache[src] = new Promise((resolve) => {
        const img = new Image();
        img.onload = () => {
          this.__cache[src] = true;
          resolve(this.__cache[src]);
        };
        img.src = src;
      }).then(() => {
        this.__cache[src] = true;
      });
    }
    if (this.__cache[src] instanceof Promise) {
      throw this.__cache[src];
    }
    return this.__cache[src];
  },
};

function TargetImage({ src, alt, ...props }) {
  imgCache.read(src);
  return <img {...props} alt={alt || ''} src={src} />;
}

export default function SuspenseImage({ src, alt, fallback: Fallback, fallbackClass = '', ...props }) {
  let fallback;
  if (typeof Fallback === 'object') {
    fallback = (
      <div {...props} className={fallbackClass}>
        {Fallback}
      </div>
    );
  } else {
    fallback = <Fallback {...props} className={fallbackClass} alt={alt} />;
  }
  return (
    <Suspense fallback={fallback}>
      <TargetImage {...props} src={src} alt={alt} />
    </Suspense>
  );
}
