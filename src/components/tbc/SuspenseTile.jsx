import React, { Suspense } from 'react';
import manifest from '../../assets/img/dummy/xl-1200/manifest.json';
import { generatePlaceholder } from '../../utility/utilities';

const { resolutions } = manifest;
const defSize = resolutions.tile;

const imgCache = {
  __cache: {},
  read(src) {
    if (!this.__cache[src]) {
      this.__cache[src] = new Promise((resolve) => {
        const img = new Image();
        img.onload = () => {
          this.__cache[src] = true;
          resolve(this.__cache[src]);
        };
        img.src = src;
      }).then(() => {
        this.__cache[src] = true;
      });
    }
    if (this.__cache[src] instanceof Promise) {
      throw this.__cache[src];
    }
    return this.__cache[src];
  },
};

const Fallback = ({ alt, className, ...props }) => (
  <div
    {...props}
    alt={alt || 'fallback-image'}
    style={{ backgroundImage: `url(${generatePlaceholder(...defSize)})` }}
    className={`tile-image ${className}`}
  />
);

function TargetTile({ srcImage, alt, className, ...props }) {
  if (!srcImage) return <Fallback {...props} alt={alt} className={className} />;

  imgCache.read(srcImage);
  return (
    <div
      {...props}
      alt={alt || ''}
      style={{
        backgroundImage: `url(${srcImage})`,
      }}
      className={`tile-image ${className}`}
    />
  );
}

export default function SuspenseTile({ srcImage, alt, className, ...props }) {
  return (
    <Suspense fallback={<Fallback {...props} alt={alt} className={className} />}>
      <TargetTile {...props} srcImage={srcImage} alt={alt} className={className} />
    </Suspense>
  );
}
